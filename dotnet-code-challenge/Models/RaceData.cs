﻿namespace dotnet_code_challenge.Models
{
    public class HorseInfo
    {
        public string HorseName { get; set; }

        public decimal Price { get; set; }
    }
}
