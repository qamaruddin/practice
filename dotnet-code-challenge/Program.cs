﻿using dotnet_code_challenge.Interfaces;
using dotnet_code_challenge.Interfaces.Implementation;
using System;

namespace dotnet_code_challenge
{
    class Program
    {
        static void Main(string[] args)
        {
            IFileReader fileReader = new FileReader();
            IHorseInfo cauFieldRaceData = new CaufieldRaceInfo(fileReader);
            IHorseInfo wolferhamptonRaceData = new WolferhamptonRaceInfo (fileReader);
            var data = wolferhamptonRaceData.GetHorseInfoOrderAsc();

            foreach (var item in data)
            {
                Console.WriteLine($"Name: {item.HorseName }, Price : {item.Price }");
            }
            Console.ReadKey();
        }
    }
}
