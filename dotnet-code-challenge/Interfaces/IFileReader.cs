﻿namespace dotnet_code_challenge.Interfaces.Interfaces
{
    public interface IFileReader
    {
        string ReadFile(string filePath);
    }
}