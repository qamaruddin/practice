﻿using dotnet_code_challenge.Models;
using System.Collections.Generic;

namespace dotnet_code_challenge.Interfaces
{
    public interface IHorseInfo
    {
        IEnumerable<HorseInfo> GetHorseInfoOrderAsc();
    }
}
