﻿using System.Collections.Generic;
using System.Linq;
using dotnet_code_challenge.Interfaces.Interfaces;
using dotnet_code_challenge.Models;
using Newtonsoft.Json.Linq;

namespace dotnet_code_challenge.Interfaces.Implementation
{
    public class WolferhamptonRaceInfo : IHorseInfo
    {
        private const string _filePath = @"FeedData\Wolferhampton_Race1.json";
        private readonly IFileReader _fileReader;

        public WolferhamptonRaceInfo(IFileReader fileReader)
        {
            _fileReader = fileReader;
        }

        public IEnumerable<HorseInfo> GetHorseInfoOrderAsc()
        {
            var fileContent = _fileReader.ReadFile(_filePath);
            var raceData = new List<HorseInfo>();
            if (string.IsNullOrEmpty(fileContent))
            {
                return raceData;
            }

            var data = JObject.Parse(fileContent);
            var selections = data.SelectToken("RawData.Markets[0].Selections")?.Value<JArray>();
            if (selections == null)
            {
                return raceData;
            }
            
            foreach (var item in selections)
            {
                var price = item.SelectToken("Price")?.Value<decimal>();
                var tags = item.SelectToken("Tags")?.Value<JObject>();
                if (tags == null)
                {
                    continue;
                }
                var name = tags.SelectToken("name")?.Value<string>();
                if (price == null || string.IsNullOrEmpty(name))
                {
                    continue;
                }
                var horseInfo = new HorseInfo() { HorseName = name, Price = price.Value };
                raceData.Add(horseInfo);
            }

            return raceData.OrderBy(x => x.Price);
        }
    }
}
