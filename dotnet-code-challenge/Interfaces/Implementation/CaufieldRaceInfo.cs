﻿using System.Collections.Generic;
using System.Linq;
using System.Xml;
using dotnet_code_challenge.Interfaces.Interfaces;
using dotnet_code_challenge.Models;

namespace dotnet_code_challenge.Interfaces.Implementation
{
    public class CaufieldRaceInfo : IHorseInfo
    {
        private const string _filePath = @"FeedData\\Caulfield_Race1.xml";
        private IFileReader _fileReader;

        public CaufieldRaceInfo(IFileReader fileReader)
        {
            _fileReader = fileReader;
        }

        public IEnumerable<HorseInfo> GetHorseInfoOrderAsc()
        {
            var fileContent = _fileReader.ReadFile(_filePath);
            var raceData = new List<HorseInfo>();
            if (string.IsNullOrEmpty(fileContent))
            {
                return raceData;
            }
            var rawXml = new XmlDocument();
            rawXml.LoadXml(fileContent);

            var horseDescElements = rawXml.SelectNodes("//race/horses/horse");
            if (horseDescElements.Count == 0)
            {
                return raceData;
            }
            var horseDesc = new Dictionary<int, string>();
            for (int i = 0; i < horseDescElements.Count; i++)
            {
                string name = horseDescElements[i].Attributes["name"]?.Value;
                var strNumber = horseDescElements[i].SelectSingleNode("number")?.InnerText;
                if (int.TryParse(strNumber, out int num))
                {
                    horseDesc.TryAdd(num, name);
                }
            }

            var horsePriceElements = rawXml.SelectNodes("//prices/price/horses/horse");
            if (horsePriceElements.Count == 0)
            {
                return null;
            }
            var horsePrices = new Dictionary<int, decimal>();
            for (int i = 0; i < horsePriceElements.Count; i++)
            {
                string strPrice = horsePriceElements[i].Attributes["Price"]?.Value;
                string strNumber = horsePriceElements[i].Attributes["number"]?.Value;
                if (int.TryParse(strNumber, out int number) && decimal.TryParse(strPrice, out decimal price))
                {
                    horsePrices.TryAdd(number, price);
                }
            }

            
            foreach (var item in horseDesc)
            {
                if (horsePrices.TryGetValue(item.Key, out decimal price))
                {
                    raceData.Add(new HorseInfo { HorseName = item.Value, Price = price });
                }
            }

            return raceData.OrderBy(x => x.Price);
        }

    }
}
