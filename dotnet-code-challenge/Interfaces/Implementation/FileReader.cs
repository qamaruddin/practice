﻿using dotnet_code_challenge.Interfaces.Interfaces;
using System;
using System.IO;

namespace dotnet_code_challenge.Interfaces.Implementation
{
    public class FileReader : IFileReader
    {
        public string ReadFile(string filePath)
        {
            if (string.IsNullOrEmpty(filePath ))
            {
                throw new ArgumentNullException("filePath", "filePath is mandatory");
            }

            return File.ReadAllText(filePath);
        }
    }
}