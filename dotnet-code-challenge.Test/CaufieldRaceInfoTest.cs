using dotnet_code_challenge.Interfaces.Implementation;
using dotnet_code_challenge.Interfaces.Interfaces;
using Moq;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using Xunit;

namespace dotnet_code_challenge.Test
{
    public class CaufieldRaceInfoTest
    {
        private string CaufieldXml = "<?xml version=\"1.0\"?><meeting xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">    <races>    <race number=\"1\" name=\"Evergreen Turf Plate\" id=\"1536514\" Status=\"OPEN\">      <horses>        <horse name=\"Advancing\" country=\"AUS\" age=\"2\" sex=\"C\" colour=\"b\" foaling_date=\"01/08/2015\" id=\"872699\">          <number>1</number>                  </horse>        <horse name=\"Coronel\" country=\"AUS\" age=\"2\" sex=\"C\" colour=\"b\" foaling_date=\"22/08/2015\" id=\"872442\">          <number>2</number>                  </horse>		<horse name=\"Fiery\" country=\"AUS\" age=\"2\" sex=\"C\" colour=\"b\" foaling_date=\"01/08/2015\" id=\"872699\">          <number>3</number>                  </horse>      </horses>      <prices>        <price>          <horses>            <horse number=\"1\" Price=\"4.2\"/>            <horse number=\"2\" Price=\"12\"/>			<horse number=\"3\" Price=\"3\"/>          </horses>        </price>      </prices>    </race>  </races></meeting>";
        private string badXml = "<?xml version=\"1.0\"?>";

        [Fact]
        public void ShouldSortByPriceAsc()
        {
            //arrange
            var fileReader = new Mock<IFileReader>();
            fileReader.Setup(x => x.ReadFile(It.IsAny<string>())).Returns(CaufieldXml);

            //act
            var result = new CaufieldRaceInfo(fileReader.Object).GetHorseInfoOrderAsc();

            //assert
            Assert.Equal(3, result.Count());
            Assert.Equal("Fiery", result.First().HorseName);

        }

        [Fact]
        public void ShouldThrowExceptionWithBadXml()
        {
            //arrange
            var fileReader = new Mock<IFileReader>();
            fileReader.Setup(x => x.ReadFile(It.IsAny<string>())).Returns(badXml);

            //act //assert
            Assert.Throws<XmlException>(() => new CaufieldRaceInfo(fileReader.Object).GetHorseInfoOrderAsc());
        }
    }
}
